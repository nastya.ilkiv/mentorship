﻿using BitcoinConverter.BAL.Models.Response;
using BitcoinConverter.BAL.Client.Interfaces;
using System;
using System.Threading.Tasks;
using BitcoinConverter.BAL.Utility;

namespace BitcoinConverter.BAL.Client
{
    public class Client : IClient
    {
        public TimeSpan TimestampOffset
        {
            get => _timestampOffset;
            set
            {
                _timestampOffset = value;
                RequestClient.SetTimestampOffset(_timestampOffset);
            }
        }
        private TimeSpan _timestampOffset;

        private readonly string _apiKey;
        private readonly string _secretKey;
        private readonly IAPIProcessor _apiProcessor;
        private readonly int _defaultReceiveWindow;

        public Client(ClientConfiguration configuration, IAPIProcessor apiProcessor = null)
        {
            Guard.AgainstNull(configuration);
            Guard.AgainstNullOrEmpty(configuration.ApiKey);
            Guard.AgainstNull(configuration.SecretKey);

            _defaultReceiveWindow = configuration.DefaultReceiveWindow;
            _apiKey = configuration.ApiKey;
            _secretKey = configuration.SecretKey;
            RequestClient.SetAPIKey(_apiKey);
            if (apiProcessor == null)
            {
                _apiProcessor = new APIProcessor(_apiKey, _secretKey);
            }
            else
            {
                _apiProcessor = apiProcessor;
            }
        }

        public async Task<SymbolPriceChangeTickerResponse> GetDailyTicker(string symbol)
        {
            Guard.AgainstNull(symbol);

            return await _apiProcessor.ProcessGetRequest<SymbolPriceChangeTickerResponse>(Endpoints.MarketData.DayPriceTicker(symbol));
        }

        public async Task<ExchangeInfoResponse> GetExchangeInfo()
        {
            return await _apiProcessor.ProcessGetRequest<ExchangeInfoResponse>(Endpoints.General.ExchangeInfo);
        }

        public async Task<ServerTimeResponse> GetServerTime()
        {
            return await _apiProcessor.ProcessGetRequest<ServerTimeResponse>(Endpoints.General.ServerTime);
        }

        public async Task<EmptyResponse> TestConnectivity()
        {
            return await _apiProcessor.ProcessGetRequest<EmptyResponse>(Endpoints.General.ServerTime);
        }

        private int SetReceiveWindow(int receiveWindow)
        {
            if (receiveWindow == -1)
            {
                receiveWindow = _defaultReceiveWindow;
            }

            return receiveWindow;
        }

        Task<EmptyResponse> IClient.TestConnectivity()
        {
            throw new NotImplementedException();
        }

        Task<ServerTimeResponse> IClient.GetServerTime()
        {
            throw new NotImplementedException();
        }

        Task<SymbolPriceResponse> IClient.GetDailyTicker(string symbol)
        {
            throw new NotImplementedException();
        }

        Task<ExchangeInfoResponse> IClient.GetExchangeInfo()
        {
            throw new NotImplementedException();
        }
    }
}
