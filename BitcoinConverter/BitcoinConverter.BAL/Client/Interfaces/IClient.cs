﻿using BitcoinConverter.BAL.Models.Response;
using System;
using System.Threading.Tasks;

namespace BitcoinConverter.BAL.Client.Interfaces
{
    public interface IClient
    {
        TimeSpan TimestampOffset { get; set; }

        Task<EmptyResponse> TestConnectivity();

        Task<ServerTimeResponse> GetServerTime();

        Task<SymbolPriceResponse> GetDailyTicker(string symbol);

        Task<ExchangeInfoResponse> GetExchangeInfo();
    }
}
