﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BitcoinConverter.BAL.Models.Response;
using BitcoinConverter.BAL.Enums;

namespace BitcoinConverter.BAL.Converter
{
    public class ExchangeInfoSymbolFilterConverter : JsonConverter
    {
        public override bool CanWrite => false;

        public override bool CanConvert(Type objectType) => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var value = jObject.ToObject<ExchangeInfoSymbolFilter>();

            ExchangeInfoSymbolFilter item = null;

            switch (value.FilterType)
            {
                case ExchangeInfoSymbolFilterType.PriceFilter:
                    item = new ExchangeInfoSymbolFilterPrice();
                    break;
                case ExchangeInfoSymbolFilterType.PercentPrice:
                    item = new ExchangeInfoSymbolFilterPercentPrice();
                    break;               
                case ExchangeInfoSymbolFilterType.PercentagePrice:
                    item = new ExchangeInfoSymbolFilterPercentagePrice();
                    break;
            }

            serializer.Populate(jObject.CreateReader(), item);
            return item;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
