using System;
using BitcoinConverter.BAL.Enums;

namespace BitcoinConverter.BAL
{
    public class EndpointData
    {
        public Uri Uri;
        public EndpointSecurityType SecurityType;

        public EndpointData(Uri uri, EndpointSecurityType securityType)
        {
            Uri = uri;
            SecurityType = securityType;
        }

        public override string ToString()
        {
            return Uri.AbsoluteUri;
        }
    }
}