﻿using System;
using System.Linq;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Enums;
using BitcoinConverter.BAL.Models.Request;
using BitcoinConverter.BAL.Models.Request.Interfaces;
using BitcoinConverter.BAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace BitcoinConverter.BAL
{
    public static class Endpoints
    {

        private static readonly JsonSerializerSettings _settings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            FloatParseHandling = FloatParseHandling.Decimal
        };

        internal static string APIBaseUrl = "https://api.binance.com/api";
        internal static string WAPIBaseUrl = "https://api.binance.com/wapi";

        private static string APIPrefix { get; } = $"{APIBaseUrl}";
        private static string WAPIPrefix { get; } = $"{WAPIBaseUrl}";

        public static class General
        {
            internal static string ApiVersion = "v1";

            public static EndpointData TestConnectivity => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/ping"), EndpointSecurityType.None);
            public static EndpointData ServerTime => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/time"), EndpointSecurityType.None);
            public static EndpointData ExchangeInfo => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/exchangeInfo"), EndpointSecurityType.None);

        }

        public static class MarketData
        {
            internal static string ApiVersion = "v1";

            public static EndpointData KlineCandlesticks(GetKlinesCandlesticksRequest request)
            {
                var queryString = GenerateQueryStringFromData(request);
                return new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/klines?{queryString}"), EndpointSecurityType.None);
            }

            public static EndpointData DayPriceTicker(string symbol)
            {
                return new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/ticker/24hr?symbol={symbol}"),
                    EndpointSecurityType.None);
            }

            public static EndpointData AllSymbolsPriceTicker => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/ticker/allPrices"), EndpointSecurityType.ApiKey);

            public static EndpointData SymbolsOrderBookTicker => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/ticker/allBookTickers"), EndpointSecurityType.ApiKey);
        }

        public static class Account
        {
            internal static string ApiVersion = "v3";

            public static EndpointData AllOrders(AllOrdersRequest request)
            {
                var queryString = GenerateQueryStringFromData(request);
                return new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/allOrders?{queryString}"), EndpointSecurityType.Signed);
            }

            public static EndpointData AccountInformation => new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/account"), EndpointSecurityType.Signed);

            public static EndpointData AccountTradeList(AllTradesRequest request)
            {
                var queryString = GenerateQueryStringFromData(request);
                return new EndpointData(new Uri($"{APIPrefix}/{ApiVersion}/myTrades?{queryString}"), EndpointSecurityType.Signed);
            }

            public static EndpointData SystemStatus()
            {
                return new EndpointData(new Uri($"{WAPIPrefix}/{ApiVersion}/systemStatus.html"), EndpointSecurityType.None);
            }
        }

        private static string GenerateQueryStringFromData(IRequest request)
        {
            if (request == null)
            {
                throw new Exception("No request data provided - query string can't be created");
            }
            var obj = (JObject)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(request, _settings), _settings);

            return String.Join("&", obj.Children()
                .Cast<JProperty>()
                .Where(j => j.Value != null)
                .Select(j => j.Name + "=" + System.Net.WebUtility.UrlEncode(j.Value.ToString())));
        }
    }
}
