﻿namespace BitcoinConverter.BAL.Enums
{
    public enum EndpointSecurityType
    {
        None,
        ApiKey,
        Signed,
    }
}