﻿namespace BitcoinConverter.BAL.Enums
{
    public enum ExchangeInfoOrderType
    {
        Limit,
        Market,
        LimitMaker,
        StopLossLimit,
        TakeProfitLimit
    }
}
