namespace BitcoinConverter.BAL.Enums
{
    public enum ExchangeInfoSymbolFilterType
    {
        #region Symbol filters

        PriceFilter,
        PercentPrice,
        LotSize,
        MinNotional,
        MarketLotSize,
        MaxNumOrders,
        MaxNumAlgoOrders,
        MaxNumIcebergOrders,

        #endregion

        #region Exchange Filters

        ExchangeMaxNumOrders,
        ExchangeMaxNumAlgoOrders,

        #endregion
        PercentagePrice,
        IcebergParts,
    }
}