﻿namespace BitcoinConverter.BAL.Enums
{
    public enum HttpVerb
    {
        GET,
        POST,
        DELETE,
        PUT,
    }
}