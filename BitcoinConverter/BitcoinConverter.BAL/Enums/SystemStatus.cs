﻿namespace BitcoinConverter.BAL.Enums
{
    public enum SystemStatus
    {
        Normal = 0,
        SystemMaintenance = 1,
    }
}