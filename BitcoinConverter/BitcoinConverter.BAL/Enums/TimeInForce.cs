﻿namespace BitcoinConverter.BAL.Enums
{
    public enum TimeInForce
    {
        GTC,
        IOC,
    }
}