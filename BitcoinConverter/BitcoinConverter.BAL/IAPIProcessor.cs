using System;
using System.Threading.Tasks;

namespace BitcoinConverter.BAL
{
    public interface IAPIProcessor
    {
        Task<T> ProcessGetRequest<T>(EndpointData endpoint, int receiveWindow = 5000) where T : class;
        Task<T> ProcessDeleteRequest<T>(EndpointData endpoint, int receiveWindow = 5000) where T : class;
        Task<T> ProcessPostRequest<T>(EndpointData endpoint, int receiveWindow = 5000) where T : class;
        Task<T> ProcessPutRequest<T>(EndpointData endpoint, int receiveWindow = 5000) where T : class;
    }
}