﻿namespace BitcoinConverter.BAL.Market
{
    public static class TradingPairSymbols
    {
        public static class USDTPairs
        {
            public static readonly string BTC_USDT = "BTCUSDT";
            public static readonly string ETH_USDT = "ETHUSDT";
            public static readonly string BNB_USDT = "BNBUSDT";
            public static readonly string BCC_USDT = "BCCUSDT";
            public static readonly string NEO_USDT = "NEOUSDT";
        }
    }
}
