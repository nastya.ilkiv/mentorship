using System;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Models.Request.Interfaces;
using Newtonsoft.Json;

namespace BitcoinConverter.BAL.Models.Request
{
    public class AccountRequest : IRequest
    {
        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime TimeStamp { get; set; }
    }
}