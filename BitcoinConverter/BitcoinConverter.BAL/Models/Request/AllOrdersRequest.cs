using BitcoinConverter.BAL.Models.Request.Interfaces;

namespace BitcoinConverter.BAL.Models.Request
{
    public class AllOrdersRequest : IRequest
    {
        public string Symbol { get; set; }
        public long? OrderId { get; set; }
        public int? Limit { get; set; }
    }
}