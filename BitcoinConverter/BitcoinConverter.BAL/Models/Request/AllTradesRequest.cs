using BitcoinConverter.BAL.Models.Request.Interfaces;

namespace BitcoinConverter.BAL.Models.Request
{
    public class AllTradesRequest : IRequest
    {
        public string Symbol { get; set; }
        public long? FromId { get; set; }
        public int? Limit { get; set; }
    }
}