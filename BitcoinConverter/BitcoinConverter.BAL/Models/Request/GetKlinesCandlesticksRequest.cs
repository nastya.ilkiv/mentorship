using System;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Enums;
using BitcoinConverter.BAL.Models.Request.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BitcoinConverter.BAL.Models.Request
{
    public class GetKlinesCandlesticksRequest: IRequest
    {
        public string Symbol { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public KlineInterval Interval { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime? StartTime { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime? EndTime { get; set; }

        public int? Limit { get; set; }

    }
}
