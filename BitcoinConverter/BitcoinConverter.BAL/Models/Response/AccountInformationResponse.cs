﻿namespace BitcoinConverter.BAL.Models.Response
{
    public class AccountInformationResponse
    {
        public int MakerCommission { get; set; }

        public int TakerCommission { get; set; }

        public int BuyerCommission { get; set; }

        public int SellerCommission { get; set; }

        public bool CanTrade { get; set; }

        public bool CanWithdraw { get; set; }

        public bool CanDeposit { get; set; }
    }
}