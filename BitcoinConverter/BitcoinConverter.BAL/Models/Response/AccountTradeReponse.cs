﻿using System;
using BitcoinConverter.BAL.Converter;
using Newtonsoft.Json;

namespace BitcoinConverter.BAL.Models.Response
{
    public class AccountTradeReponse
    {
        public long Id { get; set; }
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "qty")]
        public decimal Quantity { get; set; }

        public decimal Commission { get; set; }

        public string CommissionAsset { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime Time { get; set; }

        public bool IsBuyer { get; set; }

        public bool IsMaker { get; set; }

        public bool IsBestMatch { get; set; }

        public long OrderId { get; set; }
    }
}
