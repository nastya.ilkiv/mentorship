﻿namespace BitcoinConverter.BAL.Models.Response
{
    public class ExchangeInfoRateLimit
    {
        public string RateLimitType { get; set; }
        public string Interval { get; set; }
        public int Limit { get; set; }
    }
}
