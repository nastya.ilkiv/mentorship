﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Models.Response.Interfaces;

namespace BitcoinConverter.BAL.Models.Response
{
    public class ExchangeInfoResponse : IResponse
    {
        public string Timezone { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime ServerTime { get; set; }

        public List<ExchangeInfoRateLimit> RateLimits { get; set; }
        public List<ExchangeInfoSymbol> Symbols { get; set; }
    }
}
