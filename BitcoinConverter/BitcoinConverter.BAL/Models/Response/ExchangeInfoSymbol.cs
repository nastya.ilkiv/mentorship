﻿using System.Collections.Generic;
using Newtonsoft.Json;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Enums;
using Newtonsoft.Json.Converters;

namespace BitcoinConverter.BAL.Models.Response
{
    public class ExchangeInfoSymbol
    {
        public string Symbol { get; set; }

        public string Status { get; set; }

        public string BaseAsset { get; set; }

        public int BaseAssetPrecision { get; set; }

        public string QuoteAsset { get; set; }

        public int QuotePrecision { get; set; }

        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        public List<ExchangeInfoOrderType> OrderTypes { get; set; }

        public bool IcebergAllowed { get; set; }

        [JsonProperty(ItemConverterType = typeof(ExchangeInfoSymbolFilterConverter))]
        public List<ExchangeInfoSymbolFilter> Filters { get; set; }
    }
}
