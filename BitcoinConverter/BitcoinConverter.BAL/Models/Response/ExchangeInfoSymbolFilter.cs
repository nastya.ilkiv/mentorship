﻿using Newtonsoft.Json;
using BitcoinConverter.BAL.Enums;
using Newtonsoft.Json.Converters;

namespace BitcoinConverter.BAL.Models.Response
{
    public class ExchangeInfoSymbolFilter
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ExchangeInfoSymbolFilterType FilterType { get; set; }
    }
}
