﻿using System;
using System.Runtime.Serialization;

namespace BitcoinConverter.BAL.Models.Response
{
    [DataContract]
    public class ExchangeInfoSymbolFilterPrice : ExchangeInfoSymbolFilter
    {
        public Decimal MinPrice { get; set; }

        public Decimal MaxPrice { get; set; }

        public Decimal TickSize { get; set; }
    }
}
