﻿using System;
using BitcoinConverter.BAL.Converter;
using BitcoinConverter.BAL.Models.Response.Interfaces;
using Newtonsoft.Json;

namespace BitcoinConverter.BAL.Models.Response
{
    public class ServerTimeResponse: IResponse
    {
        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime ServerTime { get; set; }
    }
}