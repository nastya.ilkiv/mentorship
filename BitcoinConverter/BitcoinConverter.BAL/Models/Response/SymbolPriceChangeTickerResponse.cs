using System;
using BitcoinConverter.BAL.Converter;
using Newtonsoft.Json;

namespace BitcoinConverter.BAL.Models.Response
{
    public class SymbolPriceChangeTickerResponse
    {
        public decimal PriceChange { get; set; }

        public decimal PriceChangePercent { get; set; }

        [JsonProperty(PropertyName = "weightedAvgPrice")]
        public decimal WeightedAveragePercent { get; set; }

        [JsonProperty(PropertyName = "prevClosePrice")]
        public decimal PreviousClosePrice { get; set; }

        public decimal LastPrice { get; set; }

        public decimal BidPrice { get; set; }

        public decimal AskPrice { get; set; }

        public decimal OpenPrice { get; set; }

        public decimal HighPrice { get; set; }

        public decimal LowPrice { get; set; }

        public decimal Volume { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime OpenTime { get; set; }

        [JsonConverter(typeof(EpochTimeConverter))]
        public DateTime CloseTime { get; set; }

        [JsonProperty(PropertyName = "firstId")]
        public long FirstTradeId { get; set; }

        [JsonProperty(PropertyName = "lastId")]
        public long LastId { get; set; }

        [JsonProperty(PropertyName = "count")]
        public int TradeCount { get; set; }
    }
}
