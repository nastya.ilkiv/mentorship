namespace BitcoinConverter.BAL.Models.Response
{
    public class SymbolPriceResponse
    {
        public string Symbol { get; set; }

        public decimal Price { get; set; }
    }
}