﻿using BitcoinConverter.BAL.Enums;
using BitcoinConverter.BAL.Models.Response.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BitcoinConverter.BAL.Models.Response
{
    public class SystemStatusResponse : IResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public SystemStatus Status { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string Message { get; set; }
    }
}