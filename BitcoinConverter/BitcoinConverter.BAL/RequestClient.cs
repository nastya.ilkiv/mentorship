﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BitcoinConverter.BAL.Enums;

namespace BitcoinConverter.BAL
{
    internal static class RequestClient
    {
        private static readonly HttpClient HttpClient;
        private static SemaphoreSlim _rateSemaphore;
        private static int _limit = 10;
        public static int SecondsLimit = 10;
        private static string _apiKey = string.Empty;
        private static bool RateLimitingEnabled = false;
        private const string APIHeader = "X-MBX-APIKEY";
        private static readonly Stopwatch Stopwatch;
        private static int _concurrentRequests = 0;
        private static TimeSpan _timestampOffset;
        private static readonly object LockObject = new object();

        static RequestClient()
        {
            var httpClientHandler = new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            };
            HttpClient = new HttpClient(httpClientHandler);
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _rateSemaphore = new SemaphoreSlim(_limit, _limit);
            Stopwatch = new Stopwatch();
        }

        public static void SetAPIKey(string key)
        {
            if (HttpClient.DefaultRequestHeaders.Contains(APIHeader))
            {
                lock (LockObject)
                {
                    if (HttpClient.DefaultRequestHeaders.Contains(APIHeader))
                    {
                        HttpClient.DefaultRequestHeaders.Remove(APIHeader);
                    }
                }
            }
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation(APIHeader, new[] { key });
        }

        public static void SetTimestampOffset(TimeSpan time)
        {
            _timestampOffset = time;
        }

        public static async Task<HttpResponseMessage> GetRequest(Uri endpoint)
        {
            return await CreateRequest(endpoint);
        }

        public static async Task<HttpResponseMessage> PostRequest(Uri endpoint)
        {
            return await CreateRequest(endpoint, HttpVerb.POST);
        }

        public static async Task<HttpResponseMessage> DeleteRequest(Uri endpoint)
        {
            return await CreateRequest(endpoint, HttpVerb.DELETE);
        }

        public static async Task<HttpResponseMessage> PutRequest(Uri endpoint)
        {
            return await CreateRequest(endpoint, HttpVerb.PUT);
        }

        private static async Task<HttpResponseMessage> CreateRequest(Uri endpoint, HttpVerb verb = HttpVerb.GET)
        {
            Task<HttpResponseMessage> task = null;

            if (RateLimitingEnabled)
            {
                await _rateSemaphore.WaitAsync();
                if (Stopwatch.Elapsed.Seconds >= SecondsLimit || _rateSemaphore.CurrentCount == 0 || _concurrentRequests == _limit)
                {
                    var seconds = (SecondsLimit - Stopwatch.Elapsed.Seconds) * 1000;
                    var sleep = seconds > 0 ? seconds : seconds * -1;
                    Thread.Sleep(sleep);
                    _concurrentRequests = 0;
                    Stopwatch.Restart();
                }
                ++_concurrentRequests;
            }
            var taskFunction = new Func<Task<HttpResponseMessage>, Task<HttpResponseMessage>>(t =>
            {
                if (!RateLimitingEnabled) return t;
                _rateSemaphore.Release();
                if (_rateSemaphore.CurrentCount != _limit || Stopwatch.Elapsed.Seconds < SecondsLimit) return t;
                Stopwatch.Restart();
                --_concurrentRequests;
                return t;
            });
            switch (verb)
            {
                case HttpVerb.GET:
                    task = await HttpClient.GetAsync(endpoint)
                        .ContinueWith(taskFunction);
                    break;
                case HttpVerb.POST:
                    task = await HttpClient.PostAsync(endpoint, null)
                        .ContinueWith(taskFunction);
                    break;
                case HttpVerb.DELETE:
                    task = await HttpClient.DeleteAsync(endpoint)
                        .ContinueWith(taskFunction);
                    break;
                case HttpVerb.PUT:
                    task = await HttpClient.PutAsync(endpoint, null)
                        .ContinueWith(taskFunction);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(verb), verb, null);
            }
            return await task;
        }
    }
}
